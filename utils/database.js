const mysql = require('mysql2');

// const pool = mysql.createPool({
//     host: 'localhost',
//     user: 'root',
//     database: 'node-complete',
//     password: '2714584Aa'
// });

const pool = mysql.createPool({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    database: 'learning_db',
    password: '1234'
});

module.exports = pool.promise();