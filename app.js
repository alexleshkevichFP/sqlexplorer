const express = require('express');

const bodyParser = require('body-parser');
const path = require('path');

const publicPath = path.join(__dirname, 'public');

const homeRoutes = require('./routes/home');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({extended: false}))
app.use(express.json());
app.use(express.static(publicPath));

app.use(homeRoutes);

app.listen(7480);