$(function () {
   $('#queryResults').hide(300);
   $('#queryInfo').hide(300);
   $('#queryErr').hide(300);

   $('#formSubmit').click((e) => {
      e.preventDefault();

      $('#queryResults').hide(300);
      $('#queryInfo').hide(300);
      $('#queryErr').hide(300);

      const data = $('#queryForm').serialize();

      $.ajax({
         type: 'POST',
         url: '/query',
         data: data,
         success: (response) => {
            console.log(response)

            if (response.hasOwnProperty('queryInfo')) {
               const message = response.queryInfo;

               $('#queryInfo .alert-success').text(message);
               $('#queryInfo').show(300);
            } else {
               const thead = $('#resHead');
               const tbody = $('#resBody');

               thead.html('');
               tbody.html('');

               response.queryCols.forEach(item => {
                  const theadTpl = `<th scope="col">${item.name}</th>`;

                  thead.append(theadTpl);
               });

               response.queryData.forEach(item => {
                  let trow = document.createElement('tr');

                  for (let [key, value] of Object.entries(item)) {
                     const innTpl = `<td>${value}</td>`;

                     $(trow).append(innTpl)
                  }

                  tbody.append(trow);
               });

               $('#queryResults').show(300);

            }


         },
         error: (err) => {
            const message = err.responseJSON.queryErr;

            $('#queryErr .alert-danger').text(message);
            $('#queryErr').show(300);
         }
      })
   });
})