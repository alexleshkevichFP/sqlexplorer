const db = require('../utils/database');

exports.getIndex = (req, res) => {
    res.render('index');
}

exports.postQuery = (req, res) => {
    const sqlQuery = req.body.query;

    db.execute(sqlQuery)
        .then(([rows, fieldData]) => {
            const queryData = rows ? rows : [];
            const queryCols = fieldData ? fieldData : [];

            if (Array.isArray(rows)) {
                res.json({
                    queryData,
                    queryCols
                })
            } else {
                res.json({queryInfo: rows.info});
            }
        })
        .catch(err => {
            const queryErr = err.message;
            res.status(500).json({queryErr})
        });
}